<?php get_header(); ?>

<?php if ( have_rows( 'secties' ) ): ?>
	<div class="menu-container">
		<span class="site-title"><a href="<?php echo get_bloginfo('url');?>"><?php echo get_bloginfo( 'name' ); ?></a></span>
		<ul class="menu">
			<?php $i = 0;
			while ( have_rows( 'secties' ) ) : the_row();
				$i ++; ?>
				<li id="<?php echo $i; ?>"
				    data-scroll-target="<?php echo sanitizePage( get_sub_field( 'title' ) ); ?>">
					<img src="<?php echo the_sub_field( 'icon' ); ?>" alt=""></li>
			<?php endwhile; ?>
		</ul>

		<?php while ( have_rows( 'secties' ) ) : the_row(); ?>
			<img id="<?php echo sanitizePage( get_sub_field( 'title' ) ); ?>"
			     src="<?php the_sub_field( 'menu_image' ); ?>" alt="">
		<?php endwhile; ?>
	</div>

	<div class="section-menu"></div>

	<div class="section-container">
		<?php while ( have_rows( 'secties' ) ) : the_row(); ?>

			<style>
				section#<?php echo sanitizePage (get_sub_field( 'title' ) ); ?> .btn {
					background-color: <?php the_sub_field('page_color'); ?>;
				}
				section#<?php echo sanitizePage (get_sub_field( 'title' ) ); ?> .page-bg {
					background-color: <?php the_sub_field('page_color'); ?>;
				}
				section#<?php echo sanitizePage (get_sub_field( 'title' ) ); ?> .page-color {
					color: <?php the_sub_field('page_color'); ?>;
				}
				section#<?php echo sanitizePage (get_sub_field( 'title' ) ); ?> .page-border-color {
					border-color: <?php the_sub_field('page_color'); ?>;
				}
				section#<?php echo sanitizePage (get_sub_field( 'title' ) ); ?> .page-border-color input,
				section#<?php echo sanitizePage (get_sub_field( 'title' ) ); ?> .page-border-color textarea{
					border-color: <?php the_sub_field('page_color'); ?>;
				}
			</style>

			<section class="hidden <?php echo get_row_layout(); ?>"
			         id="<?php echo sanitizePage( get_sub_field( 'title' ) ); ?>">
				<div class="section-content">
					<?php if ( get_row_layout() == 'text' ): ?>
						<h1><?php echo strtoupper( get_sub_field( 'title' ) ); ?></h1>

						<p><?php the_sub_field( 'content' ); ?></p>
					<?php elseif ( get_row_layout() == 'books' ):
						$book_id = get_sub_field( 'book' ); ?>

						<div class="details">
							<img class="book-cover" src="<?php the_field( 'boek_cover', $book_id ); ?>"
							     alt="<?php echo get_the_title( $book_id ); ?>">
							<a href="<?php the_field( 'bestel_link', $book_id ); ?>"
							   class="btn btn-fullwidth">Bestellen</a>
						</div>
						<div class="sneak-peek">
							<h2><?php echo get_the_title( $book_id ); ?></h2>
							<div class="accordion">
								<div id="excerpt" class="tab">
									<div class="more">Bekijk de samenvatting <i class="fa fa-arrow-down"></i></div>
									<div class="content">
										<?php the_field( 'excerpt', $book_id ); ?>
									</div>
								</div>
								<div id="sneak-peek" class="tab">
									<div class="more">Bekijk de inkijk <i class="fa fa-arrow-down"></i></div>
									<div class="content">
										<h3>Inkijk in het boek</h3>
										<?php the_field( 'speak-peek', $book_id ); ?>
									</div>
								</div>
							</div>
						</div>

					<?php elseif ( get_row_layout() == 'photos' ):
						// WP_Query arguments
						$args = array(
							'post_type'     => array( 'album' ),
							'post_status'   => array( 'publish' ),
							'order'         => 'asc'
						);

						// The Query
						$albums = new WP_Query( $args );

						// The Loop
						if ( $albums->have_posts() ): ?>
							<div class="albums">
								<?php while ( $albums->have_posts() ): $albums->the_post();
									$cover = get_field( 'album_cover' ); ?>
								<a href="<?php echo get_permalink(); ?>" class="album b-lazy" data-src="<?php echo $cover['sizes']['thumbnail']; ?>"
									     id="<?php echo sanitizePage( get_the_title() ); ?>"
									     style="background-image: url(<?php echo $cover['sizes']['thumbnail']; ?>)">
										<h2><?php the_title(); ?></h2>
									</a>
								<?php endwhile; ?>
							</div>
						<?php else: ?>
							<p>No albums added yet..</p>
						<?php endif;
						wp_reset_postdata(); ?>

					<?php elseif ( get_row_layout() == 'contact' ): ?>

						<div class="content page-border-color">
							<?php the_sub_field( 'content' ); ?>
							<?php if(have_rows('social_media')): ?>
							<div class="social-media">
								<?php while(have_rows('social_media')): the_row();
									if(get_sub_field('social_media') == 'facebook'): ?>
										<a target="_blank" href="https://www.facebook.com/<?php the_sub_field('url');?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/Facebook_logo.png" class="social-media-icon" alt=""> <?php the_sub_field('url');?></a>
									<?php elseif(get_sub_field('social_media') == 'linkedin'): ?>
										<a target="_blank" href="https://www.linkedin.com/in/<?php the_sub_field('url');?>"><img src="<?php echo get_template_directory_uri();?>/assets/images/linkedin.png" class="social-media-icon" alt=""> <?php echo get_bloginfo('site_name'); ?></a>
									<?php endif;
								endwhile; ?>
							</div>
							<?php endif; ?>
						</div>

						<?php $id = get_sub_field( 'contactform' );
						echo do_shortcode( '[contact-form-7 id="' . $id . '" title="' . get_the_title( $id ) . '"]' );
					endif; ?>
				</div>
			</section>
		<?php endwhile; ?>
	</div>
<?php endif; ?>

<?php get_footer(); ?>