var Route = Class({
    currentPage: "",
    prevPage: "",
    homePage: "",
    pages: [],
    isMobile: false,
    clicked: false,
    screenWidth: 0,
    screenHeight: 0,
    menuHeight: 0,
    init: function () {

        // Index all sections
        this.pages = $('section').map(function () {
            if (Route.pages.indexOf($(this) === -1)) {
                return $(this).attr('id');
            }
        }).get();

        this.registerEvents();
        this.fillMenus();
        this.setScrollActions();
        this.registerAccordions();
    },

    onResize: function () {
        this.screenWidth = $(window).width();
        this.screenHeight = $(window).height();

        if(this.screenWidth <= 1380 || this.screenHeight <= 780) {
            this.menuHeight = 44;
        } else {
            this.menuHeight = 62;
        }
    },

    /**
     * Register the events
     */
    registerEvents: function () {
        $('body').on('click', '*[data-scroll-target]', this.onTargetPageClick);
    },

    /**
     * Fired when the call to action to move the client to another page
     * @param  clickEventArgs evt
     */
    onTargetPageClick: function (evt) {
        evt.preventDefault();
        Route.clicked = true;

        var value = $(this).data('scroll-target');
        this.currentPage = value;

        var id = $(this).attr('id');
        var target = $('section#' + this.currentPage);

        if (target.length) {

            $('.section-container').animate({
                scrollTop: target[0].offsetTop
            }, 1000);
            $('.section-menu .menu-item:first-of-type').css('margin-top', 124 - (this.menuHeight * id) + 'px');
            $('.menu-container > img#' + this.currentPage).addClass('active');
            $('.menu-container > img:not(#' + this.currentPage + ')').removeClass('active');
            Route.clicked = false;
            return false;
        }

    },

    sanitizePage: function (page) {
        var sanitizedPage = page.replace(' ', '-');
        sanitizedPage = page.replace('\'', '');
        sanitizedPage = page.replace('"', '');
    },

    fillMenus: function () {
        // Set standard homepage
        this.homePage = this.pages[0];
        this.currentPage = this.homePage;
        if ($('section#' + this.homePage).hasClass('hidden')) $('section#' + this.homePage).removeClass();
        $('.menu-container > img#' + this.homePage).addClass('active');

        for (var i = 0; i < this.pages.length; i++) {
            var title = this.pages[i].toUpperCase().replace('-', ' ');

            if (this.pages[i + 1] == this.currentPage) {
                $('.section-menu').append("<div id='" + (i + 1) + "' class='menu-item previous-menu-item' data-scroll-target='" + this.pages[i] + "'><a href='#" + this.pages[i] + "'>" + title + "</a></div>");
            } else if (this.pages[i] == this.currentPage) {
                $('.section-menu').append("<div id='" + (i + 1) + "' class='menu-item current-menu-item' data-scroll-target='" + this.pages[i] + "'><a href='#" + this.pages[i] + "'>" + title + "</a></div>");
            } else if (this.pages[i - 1] == this.currentPage) {
                $('.section-menu').append("<div id='" + (i + 1) + "' class='menu-item next-menu-item' data-scroll-target='" + this.pages[i] + "'><a href='#" + this.pages[i] + "'>" + title + "</a></div>");
            } else {
                $('.section-menu').append("<div id='" + (i + 1) + "' class='menu-item' data-scroll-target='" + this.pages[i] + "'><a href='#" + this.pages[i] + "'>" + title + "</a></div>");
            }
        }
    },

    setScrollActions: function () {
        var $container = $('.section-container');
        $container.scroll(function () {
            // console.log('scroll');

            $('section').each(function (i) {
                // console.log(Route.clicked);
                var id = $(this).attr('id');

                var isElementInView = Utils.isElementInView($(this), false);

                if (isElementInView) {
                    this.currentPage = id;
                    id = $('.section-menu .menu-item[data-scroll-target="' + this.currentPage + '"]').attr('id');

                    if ($('.section-menu .menu-item:first-of-type').css('margin-top') != (Route.menuHeight*2) - (Route.menuHeight * id) + 'px')
                        $('.section-menu .menu-item:first-of-type').css('margin-top', (Route.menuHeight*2) - (Route.menuHeight * id) + 'px');

                    console.log((Route.menuHeight*2) - (Route.menuHeight * id) + 'px');

                    if (!$('.menu-container > img#' + this.currentPage).hasClass('active'))
                        $('.menu-container > img#' + this.currentPage).addClass('active');

                    if($('.menu-container img').length > 0)
                        if ($('.menu-container > img:not(#' + this.currentPage + ')').hasClass('active'))
                            $('.menu-container > img:not(#' + this.currentPage + ')').removeClass('active');
                    // console.log($(this));

                    if ($(this).hasClass('hidden'))
                        $(this).removeClass('hidden');
                }
            });
        });
    },

    registerAccordions: function () {
        $('.accordion').each(function (i) {
            $('.tab', this).each(function (i) {
                $('.more', this).on('click', function () {

                    // $('.accordion .tab').removeClass('active');
                    $('.accordion .tab .content').stop().slideUp(1000);

                    $tab = $(this).parent('.tab');


                    // $tab.toggleClass('active');
                    $('.content', $tab).stop().slideToggle(1000);

                    // if ($tab.hasClass('active')) $tab.removeClass('active'); else
                });
            });
        });
    }
});

var Route = new Route();

$(document).ready(function () {
    Route.init();
    Route.onResize();

    $(window).on('resize', function () {
        Route.onResize();
    });
});
