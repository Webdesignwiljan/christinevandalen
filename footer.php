</div> <!-- End main-container -->

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/blazy.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/lightbox.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/class.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/Utils.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/route.js"></script>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObjwect']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-46109877-1', 'auto');
	ga('send', 'pageview');

</script>
<?php wp_footer(); ?>

</body>
</html>