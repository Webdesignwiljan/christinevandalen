<?php

add_filter('show_admin_bar', '__return_false');

function sanitizePage($page) {
	$sanitizedPage = str_replace( '"', '', str_replace( '\'', '', str_replace( ' ', '-', strtolower( $page ) ) ) );

	return $sanitizedPage;
}

function get_content($post_id) {
	$content_post = get_post($post_id);
	$content = $content_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}