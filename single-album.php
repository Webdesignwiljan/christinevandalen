<?php get_header();

$id = get_option( 'page_on_front' ); ?>

<?php if ( have_rows( 'secties', $id ) ):
	$cover = get_field( 'album_cover' ); ?>
	<div class="menu-container">
		<span class="site-title"><a
				href="<?php echo get_bloginfo( 'url' ); ?>"><?php echo get_bloginfo( 'name' ); ?></a></span>

		<a href="<?php echo get_bloginfo( 'url' ); ?>" class="btn page-bg back"><i class="fa fa-chevron-left"></i> Terug</a>

		<img class="active" id="<?php echo sanitizePage( the_title() ); ?>"
		     src="<?php echo $cover['sizes']['large'];?>" alt="">
	</div>


	<div class="section-menu"></div>

	<div class="section-container">
		<section class="photos" id="<?php sanitizePage(the_title()); ?>">
			<div class="section-content">
				<?php if ( have_posts() ): ?>
					<?php while ( have_posts() ): the_post(); ?>
						<div class="albums">
							<?php if ( $photos = get_field( 'photos' ) ):
								foreach ( $photos as $photo ): ?>
									<a href="<?php echo $photo['url']; ?>" data-lightbox="<?php the_title(); ?>"
									   class="album b-lazy" style="background-image: url(<?php echo $photo['sizes']['thumbnail']; ?>)" data-src="<?php echo $photo['sizes']['thumbnail']; ?>"></a>
								<?php endforeach;
							else: ?>
								<p>Helaas.. Er zijn nog geen foto's toegevoegd aan dit album..</p>
							<?php endif; ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</section>
	</div>
<?php endif; ?>

<?php get_footer(); ?>
